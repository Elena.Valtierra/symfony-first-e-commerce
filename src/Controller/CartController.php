<?php

namespace App\Controller;

use App\Form\CartType;
use App\Manager\CartManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    // public function index(SessionInterface $session): Response //Sami's version
    // public function index(CartManager $cartManager): Response
    // {
    //     $cart = $cartManager->getCurrentCart();
    //     $form = $this->createForm(CartType::class, $cart);

    //     return $this->render('cart/index.html.twig', [
    //         'cart' => $cart,
    //         'form' => $form->createView()
    //     ]);
    // }
}

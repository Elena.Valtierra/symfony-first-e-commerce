<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(): Response
    {
        $role = $this->getUser()->getRoles();
        switch ($role[0]) {
            case "ROLE_USER":
                return $this->redirectToRoute('account');
                break;
            case "ROLE_ADMIN":
                return $this->redirectToRoute('dashboard');
                break;
        }

        //This is for the view
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
